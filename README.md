# Backend - Radar IF

Projeto da disciplina de Engenharia de Software do Mestrado em TI - PPGTI 2020 do IFPB - Campus João Pessoa.

Esse repositório constitui o **backend** do sistema, sendo construído utilizando-se as tecnologias Nodejs e Mongodb.

## Preparação

Primeiramente, devem ser instalados o `node` e o `npm` para execução do projeto. Para isso, adicione o repositório do 
_NodeServer_ ao `yum` com:

```bash
$ curl -sL https://rpm.nodesource.com/setup_10.x | sudo bash -
```

Com isso, instale o `node` com :

```bash
$ sudo yum install nodejs
```

Após, verifique a instalação do `node` com `node --version` e a instalação do `npm` com `npm --version`. As saídas devem ser `v10.x.y`.

#### Dependências

Para **instalação das dependências**, execute o comando `npm i` estando na raiz do projeto. Deverá ser criada a pasta `node_modules`.

#### Execução

Para colocar o projeto em **execução**, execute o comando `npm start`. Com isso, abra o seu _browser_ e acesse `http://localhost:3000`.

### Para Execução em  Ambiente Docker

Para utilizar o projeto em container docker basta executar o comando `docker-compose up` na raiz do projeto.
*OBS.* lembre-se de configurar o docker em sua máquina.

### Para Teste

Basta acessar a url `http://localhost:3000/api/v1/info` o resultado apresenta algo desse formar `{version: '0.0.1'}`





