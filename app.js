/** importa o package.json para pegar versão projeto */
const server = require('./config/server');
const config = require('./config/config');

server.listen(config.express.port, () => {
  console.log(`Server running at http://localhost:${config.express.port}/`);
});

