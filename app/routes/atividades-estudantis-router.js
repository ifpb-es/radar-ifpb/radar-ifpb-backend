const router = require('express').Router();
const request = require('request');
const dataset = require('./const.dataset');
const mensagens = require('../utils/mensagens');

const programaController = require('../controllers/programa.controller');
const bolsaController = require('../controllers/bolsa.controller');

const path = {
  urlApi: '/atividades/estudantis',
  sinconizar: '/sincronizacao',
  programas: '/programas',
  bolsas: '/bolsas'
};

/**
 * @swagger
 * definitions:
 *   Mensagem:
 *     properties:
 *       mensagem:
 *         type: string
 *   TotalProgramas:
 *     properties:
 *       categoria:
 *         type: string
 *       total:
 *         type: integer
 *       atendimentos:
 *         type: object
 *   TotalProgramaCampus:
 *     properties:
 *       campus:
 *         type: string
 *       total:
 *         type: integer
 *   TotalBolsasValores:
 *     properties:
 *       categoria:
 *         type: string
 *       valorTotal:
 *         type: integer
 */

 /**
 * @swagger
 * /atividades/estudantis/sincronizacao/programas:
 *   get:
 *     tags:
 *       - Atividades Estudantis 
 *     description: Executa a sincronização dos dados aberto do ifpb para o dataset de programas estudantis
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Mensagem de sucesso
 *          schema:
 *             $ref: '#/definitions/Mensagem'
 *       500:
 *          description: Mensagem de erro
 *          schema:
 *             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}${path.sinconizar}${path.programas}`, function (req, res) {
  console.info(">>>> Iniciando a sincronização");
  request({
    url: dataset.programaEstudantis,
    json: true
  }, function (error, response, body) {
    if (!error && response.statusCode === 200) {
      programaController.sincronizar(req,res,body);
    } else {
      res.send({ mensagem: mensagens.errors.sincronizar });
    }
  });
});

/**
 * @swagger
 * /atividades/estudantis/programas:
 *   get:
 *     tags:
 *       - Atividades Estudantis 
 *     description: Recupera os programas estudantis oferecidos.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Array com o resultado da consulta. 
 *          schema:
 *             $ref: '#/definitions/TotalProgramas'
 *       500:
 *          description: Mensagem de erro
 *          schema:
 *             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}${path.programas}`, function (req, res) {
  programaController.consultarProgramasEstudantis(res);
});

/**
 * @swagger
 * /atividades/estudantis/programas/campus:
 *   get:
 *     tags:
 *       - Atividades Estudantis 
 *     description: Recupera a quantidade de programas estudantis oferecidos por campus.
 *     produces:
 *       - application/json 
 *     responses:
 *       200:
 *          description: Array com o resultado da consulta. 
 *          schema:
 *             $ref: '#/definitions/TotalProgramaCampus'
 *       500:
 *          description: Mensagem de erro
 *          schema:
 *             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}${path.programas}/campus`, function (req, res) {
  programaController.consultarProgramasPorCampus(res);
});


/**
 * @swagger
 * /atividades/estudantis/sincronizacao/bolsas:
 *   get:
 *     tags:
 *       - Atividades Estudantis 
 *     description: Executa a sincronização dos dados aberto do ifpb para o dataset de bolsas estudantis
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Mensagem de sucesso
 *          schema:
 *             $ref: '#/definitions/Mensagem'
 *       500:
 *          description: Mensagem de erro
 *          schema:
 *             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}${path.sinconizar}${path.bolsas}`, function (req, res) {
  console.info(">>>> Iniciando a sincronização");
  request({
    url: dataset.bolsasEstudantis,
    json: true
  }, function (error, response, body) {
    if (!error && response.statusCode === 200) {
      bolsaController.sincronizar(req,res,body);
    } else {
      res.send({ mensagem: mensagens.errors.sincronizar });
    }
  });
});



/**
 * @swagger
 * /atividades/estudantis/bolsas/valores:
 *   get:
 *     tags:
 *       - Atividades Estudantis 
 *     description: Recupera o valor total gasto com cada tipo de bolsas
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Array com o resultado da consulta. 
 *          schema:
 *             $ref: '#/definitions/TotalBolsasValores'
 *       500:
 *          description: Mensagem de erro
 *          schema:
 *             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}${path.bolsas}/valores`, function (req, res) {
  bolsaController.consultarValoresBolsas(res);
});

module.exports = router;