const router = require('express').Router();
const request = require('request');
const dataset = require('./const.dataset');
const mensagens = require('../utils/mensagens');

const patrimonioController = require('../controllers/patrimonio.controller');

const path = {
  urlApi: '/administracao',
  sinconizar: '/sincronizacao'
};

/**
 * @swagger
 * definitions:
 *   Mensagem:
 *     properties:
 *       mensagem:
 *         type: string
 *   TotalPatrimonioPorSituacao: 
 *     properties:
 *       situacao:
 *         type: string
 *       total:
 *         type: integer
 *   TotalPatrimonioPorEstadoConservacao: 
 *     properties:
 *       estado:
 *         type: string
 *       total:
 *         type: integer
 *   TotalPatrimonioPorCampus: 
 *     properties:
 *       campus:
 *         type: string
 *       total:
 *         type: integer
 */

/**
* @swagger
* /administracao/sincronizacao?ano=:
*   get:
*     tags:
*       - Administração 
*     description: Executa a sincronização dos dados aberto do ifpb para o dataset de patrimônio
*     produces:
*       - application/json
*     parameters:
*       - name: ano
*         description: Ano do dataset para sincronizar (2015 ou 2019)
*         in: query
*         required: true
*         type: string
*     responses:
*       200:
*          description: Mensagem de sucesso
*          schema:
*             $ref: '#/definitions/Mensagem'
*       500:
*          description: Mensagem de erro
*          schema:
*             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}${path.sinconizar}`, function (req, res) {
  console.info(">>>> Iniciando a sincronização", req.query);
  let dataSetUrl = '';
  if (req.query && req.query.ano === '2015') {
    dataSetUrl = dataset.admistracao2015a2018;
  } else if (req.query && req.query.ano === '2019') {
    dataSetUrl = dataset.admistracao2019;
  } else {
    dataSetUrl = dataset.admistracao2015a2018;
  }

  request({
    url: dataSetUrl,
    json: true
  }, function (error, response, body) {
    if (!error && response.statusCode === 200) {
      patrimonioController.sincronizar(req, res, body);
    } else {
      res.send({ mensagem: mensagens.errors.sincronizar });
    }
  });

});


/**
 * @swagger
 * /administracao/patrimonios/situacoes?campus=:
 *   get:
 *     tags:
 *       - Administração
 *     description: Recupera a quantidade de patrimônio por situação de todos os campus
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: campus
 *         description: Nome do campus para pesquisar
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *          description: Array com o resultado da consulta. 
 *          schema:
 *             $ref: '#/definitions/TotalPatrimonioPorSituacao'
 *       500:
 *          description: Mensagem de erro
 *          schema:
 *             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}/patrimonios/situacoes`, function (req, res) {
  patrimonioController.consultarPatrimonioPorSituacao(req,res);
});

/**
 * @swagger
 * /administracao/patrimonios/estado-conservacao?campus=:
 *   get:
 *     tags:
 *       - Administração
 *     description: Recupera a quantidade de patrimônio por estado de conservação de todos os campus
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: campus
 *         description: Nome do campus para pesquisar
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *          description: Array com o resultado da consulta. 
 *          schema:
 *             $ref: '#/definitions/TotalPatrimonioPorEstadoConservacao'
 *       500:
 *          description: Mensagem de erro
 *          schema:
 *             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}/patrimonios/estado-conservacao`, function (req, res) {
  patrimonioController.consultarPatrimonioPorEstadoConservacao(req,res);
});

/**
 * @swagger
 * /administracao/patrimonios/valores/inicial:
 *   get:
 *     tags:
 *       - Administração
 *     description: Recupera os valores de iniciais dos patrimônios por campus
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Array com o resultado da consulta. 
 *          schema:
 *             $ref: '#/definitions/TotalPatrimonioPorCampus'
 *       500:
 *          description: Mensagem de erro
 *          schema:
 *             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}/patrimonios/valores/inicial`, function (req, res) {
  patrimonioController.consultarValoresIniciaisDePatrimonioPorCampus(req,res);
});


/**
 * @swagger
 * /administracao/patrimonios/valores/liquido:
 *   get:
 *     tags:
 *       - Administração
 *     description: Recupera os valores de líquidos contábeis dos patrimônios por campus
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Array com o resultado da consulta. 
 *          schema:
 *             $ref: '#/definitions/TotalPatrimonioPorCampus'
 *       500:
 *          description: Mensagem de erro
 *          schema:
 *             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}/patrimonios/valores/liquido`, function (req, res) {
  patrimonioController.consultarValoresLiquidoDoPatrimonioPorCampus(req,res);
});

module.exports = router;