const router = require('express').Router();
const request = require('request');
const dataset = require('./const.dataset');
const mensagens = require('../utils/mensagens');
const projetoPesquisaController = require('../controllers/projeto-pesquisa.controller');

const path = {
  urlApi: '/projetos/pesquisas',
  sinconizar: 'sincronizacao'
};

/**
 * @swagger
 * definitions:
 *   Mensagem:
 *     properties:
 *       mensagem:
 *         type: string
 *   TotalProjetoCampus:
 *     properties:
 *       campus:
 *         type: string
 *       total:
 *         type: integer
 *   TotalProjetoAreaConhecimento:
 *     properties:
 *       areaConhecimento:
 *         type: string
 *       total:
 *         type: integer
 *   TotalProjetoAno:
 *     properties:
 *       ano:
 *         type: integer
 *       total:
 *         type: integer
 *   TotalProjetoParticipantes:
 *     properties:
 *       total:
 *         type: integer
 */

/**
 * @swagger
 * /projetos/pesquisas/sincronizacao:
 *   get:
 *     tags:
 *       - Pesquisa
 *     description: Executa a sincronização dos dados aberto do ifpb para o dataset de projetos de pesquisa
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Mensagem de sucesso
 *          schema:
 *             $ref: '#/definitions/Mensagem'
 *       500:
 *          description: Mensagem de erro
 *          schema:
 *             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}/${path.sinconizar}`, function (req, res) {
  request({
    url: dataset.projetoPesquisa,
    json: true
  }, function (error, response, body) {
    if (!error && response.statusCode === 200) {
      projetoPesquisaController.sincronizar(req, res, body);
    } else {
      res.send({ mensagem: mensagens.errors.sincronizar });
    }
  });
});

/**
 * @swagger
 * /projetos/pesquisas/campus:
 *   get:
 *     tags:
 *       - Pesquisa
 *     description: Retornar a quantidade de projetos de pesquisa por campus em todos os anos
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Array com o resultado da consulta. 
 *          schema:
 *             $ref: '#/definitions/TotalProjetoCampus'
 *       500:
 *          description: Mensagem de erro
 *          schema:
 *             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}/campus`, function (req, res) {
  projetoPesquisaController.consultarProjetosPorCampus(req, res);
});

/**
 * @swagger
 * /projetos/pesquisas/areas/conhecimentos?campus=:
 *   get:
 *     tags:
 *       - Pesquisa
 *     description: Retornar a quantidade de projetos de pesquisa por área de conhecimento em todos os anos
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: campus
 *         description: Nome do campus para consultar 
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *          description: Array com o resultado da consulta. 
 *          schema:
 *             $ref: '#/definitions/TotalProjetoAreaConhecimento'
 *       500:
 *          description: Mensagem de erro
 *          schema:
 *             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}/areas/conhecimentos`, function (req, res) {
  projetoPesquisaController.consultarProjetosPorAreaConhecimento(req, res);
});

/**
 * @swagger
 * /projetos/pesquisas/ano?campus=:
 *   get:
 *     tags:
 *       - Pesquisa
 *     description: Retornar a quantidade de projetos de pesquisa por ano
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: campus
 *         description: Nome do campus para consultar 
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *          description: Array com o resultado da consulta. 
 *          schema:
 *             $ref: '#/definitions/TotalProjetoAno'
 *       500:
 *          description: Mensagem de erro
 *          schema:
 *             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}/ano`, function (req, res) {
  projetoPesquisaController.consultarProjetosPorPeriodo(req, res);
});


/**
 * @swagger
 * /projetos/pesquisas/participantes?campus=:
 *   get:
 *     tags:
 *       - Pesquisa
 *     description: Retornar a quantidade de participantes de projetos de cada campus
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: campus
 *         description: Nome do campus para consultar 
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *          description: Array com o resultado da consulta. 
 *          schema:
 *             $ref: '#/definitions/TotalProjetoParticipantes'
 *       500:
 *          description: Mensagem de erro
 *          schema:
 *             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}/participantes`, function (req, res) {
  projetoPesquisaController.consultarQuantidadeParticipantesPorCampus(req, res);
});

module.exports = router;
