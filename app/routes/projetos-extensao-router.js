const router = require('express').Router();
const request = require('request');
const dataset = require('./const.dataset');
const mensagens = require('../utils/mensagens');
const projetoExtensaoController = require('../controllers/projeto-extensao.controller');

const path = {
  urlApi: '/projetos/extensao',
  sinconizar:'sincronizacao'
};

/**
 * @swagger
 * definitions:
 *   Mensagem:
 *     properties:
 *       mensagem:
 *         type: string
 *   TotalProjetoCampus:
 *     properties:
 *       campus:
 *         type: string
 *       total:
 *         type: integer
 *   TotalProjetoAreaConhecimento:
 *     properties:
 *       areaConhecimento:
 *         type: string
 *       total:
 *         type: integer
 *   TotalProjetoAno:
 *     properties:
 *       ano:
 *         type: integer
 *       total:
 *         type: integer
 *   TotalProjetoParticipantes:
 *     properties:
 *       total:
 *         type: integer
 *   TotalBolsasPorAno:
 *     properties:
 *       ano:
 *         type: integer
 *       total:
 *         type: integer
 */

 /**
 * @swagger
 * /projetos/extensao/sincronizacao:
 *   get:
 *     tags:
 *       - Extensão
 *     description: Executa a sincronização dos dados aberto do ifpb para o dataset de projetos de extensão
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Mensagem de sucesso
 *          schema:
 *             $ref: '#/definitions/Mensagem'
 *       500:
 *          description: Mensagem de erro
 *          schema:
 *             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}/${path.sinconizar}`, function (req, res) {
  request({
    url: dataset.projetoExtensao,
    json: true
  }, function (error, response, body) {
    if (!error && response.statusCode === 200) {
      projetoExtensaoController.sincronizar(req,res,body);
    } else {
      res.send({ mensagem: mensagens.errors.sincronizar });
    }
  });
  
});

/**
 * @swagger
 * /projetos/extensao/campus:
 *   get:
 *     tags:
 *       - Extensão
 *     description: Retornar a quantidade de projetos de extensão por campus em todos os anos
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Array com o resultado da consulta. 
 *          schema:
 *             $ref: '#/definitions/TotalProjetoCampus'
 *       500:
 *          description: Mensagem de erro
 *          schema:
 *             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}/campus`, function (req, res) {
  projetoExtensaoController.consultarProjetosPorCampus(req,res); 
});

/**
 * @swagger
 * /projetos/extensao/areas/conhecimentos?campus=:
 *   get:
 *     tags:
 *       - Extensão
 *     description: Retornar a quantidade de projetos de extensão por área de conhecimento em todos os anos
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: campus
 *         description: Nome do campus para consultar 
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *          description: Array com o resultado da consulta. 
 *          schema:
 *             $ref: '#/definitions/TotalProjetoAreaConhecimento'
 *       500:
 *          description: Mensagem de erro
 *          schema:
 *             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}/areas/conhecimentos`, function (req, res) {
  projetoExtensaoController.consultarProjetosPorAreaConhecimento(req,res); 
});

/**
 * @swagger
 * /projetos/extensao/ano?campus=:
 *   get:
 *     tags:
 *       - Extensão
 *     description: Retornar a quantidade de projetos de extensão por ano
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: campus
 *         description: Nome do campus para consultar 
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *          description: Array com o resultado da consulta. 
 *          schema:
 *             $ref: '#/definitions/TotalProjetoAno'
 *       500:
 *          description: Mensagem de erro
 *          schema:
 *             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}/ano`, function (req, res) {
  projetoExtensaoController.consultarProjetosPorPeriodo(req,res); 
});


/**
 * @swagger
 * /projetos/extensao/participantes?campus=:
 *   get:
 *     tags:
 *       - Extensão
 *     description: Retornar a quantidade de participantes de projetos de cada campus
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: campus
 *         description: Nome do campus para consultar 
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *          description: Array com o resultado da consulta. 
 *          schema:
 *             $ref: '#/definitions/TotalProjetoParticipantes'
 *       500:
 *          description: Mensagem de erro
 *          schema:
 *             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}/participantes`, function (req, res) {
  projetoExtensaoController.consultarQuantidadeParticipantesPorCampus(req, res);
});


/**
 * @swagger
 * /projetos/extensao/bolsas?campus=:
 *   get:
 *     tags:
 *       - Extensão
 *     description: Retornar os valores investidos em bolsas de projetos de cada campus por cada ano
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: campus
 *         description: Nome do campus para consultar 
 *         in: query
 *         required: false
 *         type: string
 *     responses:
 *       200:
 *          description: Array com o resultado da consulta. 
 *          schema:
 *             $ref: '#/definitions/TotalBolsasPorAno'
 *       500:
 *          description: Mensagem de erro
 *          schema:
 *             $ref: '#/definitions/Mensagem'
*/
router.get(`${path.urlApi}/bolsas`, function (req, res) {
  projetoExtensaoController.consultarValoresInvestimentoBolsas(req, res);
});

module.exports = router;
