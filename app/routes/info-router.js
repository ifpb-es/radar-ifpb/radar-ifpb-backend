const router = require('express').Router();
const pjson = require('../../package.json');

/**
 * @swagger
 * /info:
 *   get:
 *     tags:
 *       - Versão
 *     description: Retorna a versão atual do backend
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 */
router.get(`/info`, function (req, res) {
    res.send({ version: 'v.'+ pjson.version });
});

module.exports = router;