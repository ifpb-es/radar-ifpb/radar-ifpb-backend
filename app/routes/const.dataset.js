const urlDataset = {
    programaEstudantis: 'https://dados.ifpb.edu.br/dataset/b8167dd8-f1c6-41a0-90ba-8e4df6d380d8/resource/173d1ef4-319f-4666-ac76-9459c83adc45/download/programas.json',
    bolsasEstudantis: 'https://dados.ifpb.edu.br/dataset/5e8c6175-55c2-4498-b51b-7b45ae16ddcd/resource/c1d51a0e-75b9-4d83-8449-ba9ada47b453/download/bolsas.json',
    projetoPesquisa: 'https://dados.ifpb.edu.br/dataset/e99b5cfd-f2f3-4b54-bb4f-6ddd9e480af7/resource/f81de6aa-afde-4cdb-bee0-a9ed2f87c4c3/download/projetos-pesquisa.json',
    projetoExtensao: 'https://dados.ifpb.edu.br/dataset/029b50a4-f50a-422d-867f-b457277b5168/resource/d3b1908b-e6d6-4437-aefb-281b7b1b57ea/download/projetos-extensao.json',
    admistracao2015a2018: 'https://dados.ifpb.edu.br/dataset/4edc20c5-bf7d-43a3-bb00-8669ddf1cc4d/resource/d3a797da-3d3b-4f78-8698-616df7b14cf0/download/patrimonio-2015-2018.json',
    admistracao2019: 'https://dados.ifpb.edu.br/dataset/4edc20c5-bf7d-43a3-bb00-8669ddf1cc4d/resource/7841787a-8e9d-4a93-b9ed-2d583057b9a0/download/patrimonio-2019.json'
};

module.exports = urlDataset;

