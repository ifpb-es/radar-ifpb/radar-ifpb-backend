exports.converterStringParaDate = (dateStr) => {
    if (dateStr) {
        const [day, month, year] = dateStr.split('/');
        return new Date(parseInt(year, 0), parseInt(month, 0) - 1, parseInt(day, 0));
    }
}

