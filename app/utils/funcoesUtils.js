exports.handlerError  = (res, code, mensagemParam) => {
    res.status(code).json({ mensagem: mensagemParam });
};

exports.validarRetornoConsulta = (object) =>{
    return object && object.length > 0 ? true : false;
};

exports.setParametroCampusQuery = (query, param) =>{
    if (param && param.campus) {
        query.push({ '$match': { 'uo.nome': param.campus } });
    }
}