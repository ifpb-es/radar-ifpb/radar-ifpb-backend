
const mensagens = {
    errors:{
        sincronizar:'Erro ao sincronizar dataset.',
        generico:'Erro ao executar consulta.'
    },
    sucesso:{
        sincronizar:'Dados sincronizados com sucesso.',
        nenhumResultado:'Nenhum resultado encontrado.'
    }    
};

module.exports = mensagens;

