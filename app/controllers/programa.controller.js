const Programa = require('../models/programa.model');
const funcoesUtils = require('../utils/funcoesUtils');
const mensagens = require('../utils/mensagens');

exports.sincronizar = (req, res, dados) => {
    const programas = [];
    dados.forEach(item => {
        programas.push({
            _id: item.uuid,
            url: item.url,
            uuid: item.uuid,
            titulo: item.titulo,
            numero_atendimento: item.numero_atendimento,
            atendimento_por_campus: item.atendimento_por_campus,
            periodos_inscricao_por_campus: item.periodos_inscricao_por_campus
        });
    });

    Programa.collection.insertMany(programas).then(() => {
        res.status(200).json({ mensagem: mensagens.sucesso.sincronizar });
    }).catch(error => {
        console.log('errro durante a sincronização>>>>', error);
        funcoesUtils.handlerError(res, 500, mensagens.errors.sincronizar);
    });
};


exports.consultarProgramasEstudantis = (res) => {
    Programa.find({}, function (err, data) {
        if (err) {
            console.log('error:', JSON.stringify(err));
            res.json({ mensangem: mensagens.errors.generico });
            throw err;
        } else {
            if (funcoesUtils.validarRetornoConsulta(data)) {
                const resultado = data.map(item => ({
                    categoria: item.titulo,
                    total: item.numero_atendimento,
                    atendimentos: item.atendimento_por_campus
                }));
                res.json(resultado);
            } else {
                funcoesUtils.handlerError(res, 200, mensagens.sucesso.nenhumResultado);
            }
        }
    });
};

exports.consultarProgramasPorCampus = (res) => {
    Programa.find({},{ _id:0, atendimento_por_campus:1 }, function (err, data) {
        if (err) {
            console.log('error:', JSON.stringify(err));
            res.json({ mensangem: mensagens.errors.generico });
            throw err;
        } else {
            if (funcoesUtils.validarRetornoConsulta(data)) {
                let resultado = new Map();
                _montarRetornoConsultarPorCampus(data,resultado);  
                const dataSend =[];
                for (const [key, value] of resultado) {
                    dataSend.push({campus: key, total: value})
                }
                res.send(dataSend);
            } else {
                funcoesUtils.handlerError(res, 200, mensagens.sucesso.nenhumResultado);
            }
        }
    });
};


async function _montarRetornoConsultarPorCampus(data, resultado){
    for(const d of data){
        const itemArray = JSON.stringify(d.atendimento_por_campus).replace(/[{}"]/gi,'').split(',');

        if (itemArray && itemArray.length > 0) {

            if (resultado.size === 0) {

                itemArray.forEach(item => {
                    const i = item.split(':');
                    resultado.set(i[0].trim(), Number(i[1]));
                });

            } else {
                itemArray.forEach(item => {
                    const itemMap = item.split(':');
                    if (resultado.has(itemMap[0])) {
                        let valorAtual = Number(resultado.get(itemMap[0]));
                        valorAtual += Number(itemMap[1]);
                        resultado.set(itemMap[0].trim(), valorAtual);
                    } else {
                        resultado.set(itemMap[0].trim(), itemMap[1]);
                    }
                });
            }

        }

    }

}
