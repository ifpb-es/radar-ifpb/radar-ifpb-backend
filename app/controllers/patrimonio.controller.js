const Patrimonio = require('../models/patrimonio.model');
const mensagens = require('../utils/mensagens');
const funcoesUtils = require('../utils/funcoesUtils');

exports.sincronizar = (req, res, dados) => {

    const patrimonios = [];
    dados.forEach(item => {
        patrimonios.push({
            _id:item.uuid,
            url: item.url,
            uuid: item.uuid,
            numero: item.numero,
            situacao: item.situacao,
            descricao: item.descricao,
            estado_conservacao: item.estado_conservacao,
            campus: item.campus,
            valor_inicial: item.valor_inicial,
            valor_liquido_contabil: item.valor_liquido_contabil
        });
    });

    Patrimonio.collection.insertMany(patrimonios).then(() => {
        res.status(200).json({ mensagem: mensagens.sucesso.sincronizar });
    }).catch(error => {
        console.log('errro durante a sincronização>>>>', error);
        funcoesUtils.handlerError(res, 500, mensagens.errors.sincronizar);
    });
};


exports.consultarPatrimonioPorSituacao = (req, res) => {
    const param = req.query;
    const query = [];
    setParametroCampusQuery(query, param);
    query.push( {'$group' : {_id:'$situacao', total:{$sum:1}}});

    Patrimonio.aggregate([
      query
    ], function (err, data) {
        if (err) {
            console.log('error:', JSON.stringify(err));
            res.json({ mensangem: mensagens.errors.generico });
            throw err;
        } else {
            if (funcoesUtils.validarRetornoConsulta(data)) {
                const resultado = data.map(item => ({
                    situacao: item._id.toUpperCase(),
                    total: item.total
                }));
                res.json(resultado);
            } else {
                funcoesUtils.handlerError(res, 200, mensagens.sucesso.nenhumResultado);
            }
        }
    });
};

// db.patrimonios.aggregate([
//     {"$match" : { "campus.nome":"CAMPUS CAMPINA GRANDE"}},
//     {"$group" : {_id:"$estado_conservacao", total:{$sum:1}}}
// ])

exports.consultarPatrimonioPorEstadoConservacao = (req, res) => {
    const param = req.query;
    const query = [];
    setParametroCampusQuery(query, param);
    query.push( {'$group' : {_id:'$estado_conservacao', total:{$sum:1}}});
    Patrimonio.aggregate([
      query
    ], function (err, data) {
        if (err) {
            console.log('error:', JSON.stringify(err));
            res.json({ mensangem: mensagens.errors.generico });
            throw err;
        } else {
            if (funcoesUtils.validarRetornoConsulta(data)) {
                const resultado = data.map(item => ({
                    estado: item._id.toUpperCase().trim().length > 0 ? item._id.toUpperCase() : 'NÃO INFORMADO',
                    total: item.total
                }));
                res.json(resultado);
            } else {
                funcoesUtils.handlerError(res, 200, mensagens.sucesso.nenhumResultado);
            }
        }
    });
};


exports.consultarValoresIniciaisDePatrimonioPorCampus = (req, res) => {
    Patrimonio.aggregate([
        {'$group' : { _id:'$campus.nome', total: { $sum: '$valor_inicial'} }}
    ], function (err, data) {
        if (err) {
            console.log('error:', JSON.stringify(err));
            res.json({ mensangem: mensagens.errors.generico });
            throw err;
        } else {
            if (funcoesUtils.validarRetornoConsulta(data)) {
                const resultado = data.map(item => ({
                    campus: item._id,
                    total: item.total
                }));
                res.json(resultado);
            } else {
                funcoesUtils.handlerError(res, 200, mensagens.sucesso.nenhumResultado);
            }
        }
    });
};



exports.consultarValoresLiquidoDoPatrimonioPorCampus = (req, res) => {
    Patrimonio.aggregate([
        {'$group' : { _id:'$campus.nome', total: { $sum: '$valor_liquido_contabil'} }}
    ], function (err, data) {
        if (err) {
            console.log('error:', JSON.stringify(err));
            res.json({ mensangem: mensagens.errors.generico });
            throw err;
        } else {
            if (funcoesUtils.validarRetornoConsulta(data)) {
                const resultado = data.map(item => ({
                    campus: item._id,
                    total: item.total
                }));
                res.json(resultado);
            } else {
                funcoesUtils.handlerError(res, 200, mensagens.sucesso.nenhumResultado);
            }
        }
    });
};

function setParametroCampusQuery(query, param) {
    if (param && param.campus) {
        query.push({ '$match': { 'campus.nome': param.campus } });
    }
}
