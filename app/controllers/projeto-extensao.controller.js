const Extensao = require('../models/projeto-extensao.model');
const dataUtils = require('../utils/dataUtils');
const mensagens = require('../utils/mensagens');
const funcoesUtils = require('../utils/funcoesUtils');

exports.sincronizar = (req, res, dados) => {
    const projetos = [];
    dados.forEach(item => {
        projetos.push({
            url: item.url,
            uuid: item.uuid,
            titulo: item.titulo,
            uo: item.uo,
            inicio_execucao: dataUtils.converterStringParaDate(item.inicio_execucao),
            fim_execucao: dataUtils.converterStringParaDate(item.fim_execucao),
            foco_tecnologico: item.foco_tecnologico,
            area_conhecimento: item.area_conhecimento,
            resumo: item.resumo,
            justificativa: item.justificativa,
            participantes: item.participantes,
            valor_total_executado: item.valor_total_executado,
            aprovado: item.aprovado
        });
    });

    Extensao.collection.insertMany(projetos).then(() => {
        res.status(200).json({ mensagem: mensagens.sucesso.sincronizar });
    }).catch(error => {
        console.log('errro durante a sincronização>>>>', error);
        funcoesUtils.handlerError(res, 500, mensagens.errors.sincronizar);
    });
};

exports.consultarProjetosPorCampus = (req, res) => {
    Extensao.aggregate([
        { '$group': { _id: '$uo.nome', count: { $sum: 1 } } }
    ], function (err, data) {
        if (err) {
            console.log('error:', JSON.stringify(err));
            res.json({ mensangem: mensagens.errors.generico });
            throw err;
        } else {
            if (JSON.stringify(data).length > 0) {
                const resultado = [];
                data.forEach((item) => {
                    resultado.push({ campus: item._id, total: item.count });
                });
                res.json(resultado);
            }
            else {
                res.json({ mensagem: mensagens.sucesso.nenhumResultado });
            }
        }
    });
};

exports.consultarProjetosPorAreaConhecimento = (req, res) => {
    const param = req.query;
    const query = [];
    funcoesUtils.setParametroCampusQuery(query, param);
    query.push({ '$group': { _id: '$area_conhecimento', count: { $sum: 1 } } });

    Extensao.aggregate([
      query
    ], function (err, data) {
        if (err) {
            console.log('error:', JSON.stringify(err));
            funcoesUtils.handlerError(res, 500, mensagens.errors.generico);
            throw err;
        } else {
            if (funcoesUtils.validarRetornoConsulta(data)) {
                const resultado = [];
                data.forEach((item) => {
                    resultado.push({ areaConhecimento: item._id, total: item.count });
                });
                res.json(resultado);
            }
            else {
                funcoesUtils.handlerError(res, 200, mensagens.sucesso.nenhumResultado);
            }
        }
    });
};

exports.consultarProjetosPorPeriodo = (req, res) => {
    const param = req.query;
    const query = [];
    funcoesUtils.setParametroCampusQuery(query, param);
    query.push({ '$group': { _id: { '$year': '$inicio_execucao', '$year': '$fim_execucao' }, count: { $sum: 1 } } });

    Extensao.aggregate([
        query
    ], function (err, data) {
        if (err) {
            console.log('error:', JSON.stringify(err));
            funcoesUtils.handlerError(res, 500, mensagens.errors.generico);
            throw err;
        } else {
            if (funcoesUtils.validarRetornoConsulta(data)) {
                const resultado = [];
                data.forEach((item) => {
                    resultado.push({ ano: item._id, total: item.count });
                });
                res.json(resultado);
            }
            else {
                funcoesUtils.handlerError(res, 200, mensagens.sucesso.nenhumResultado);
            }
        }
    });
};



exports.consultarQuantidadeParticipantesPorCampus = (req, res) => {
    const param = req.query;
    const query = [];
    funcoesUtils.setParametroCampusQuery(query, param);
    query.push({ '$group': { _id: '$participantes.length', count: { $sum: 1 } } });
    Extensao.aggregate([
        query
    ], function (err, data) {
        if (err) {
            console.log('error:', JSON.stringify(err));
            funcoesUtils.handlerError(res, 500, mensagens.errors.generico);
            throw err;
        } else {
            if (funcoesUtils.validarRetornoConsulta(data)) {
                const resultado = [];
                data.forEach((item) => {
                    resultado.push({total: item.total });
                });
                res.json(resultado);
            }
            else {
                funcoesUtils.handlerError(res, 200, mensagens.sucesso.nenhumResultado);
            }
        }
    });
};


exports.consultarValoresInvestimentoBolsas = (req, res) => {
    const param = req.query;
    const query = [];
    funcoesUtils.setParametroCampusQuery(query, param);
    query.push({'$group' : { _id:{ $year: '$inicio_execucao', $year: '$fim_execucao' }, total: { $sum: '$valor_total_executado'} }});
    Extensao.aggregate([
        query
    ], function (err, data) {
        if (err) {
            console.log('error:', JSON.stringify(err));
            funcoesUtils.handlerError(res, 500, mensagens.errors.generico);
            throw err;
        } else {
            if (funcoesUtils.validarRetornoConsulta(data)) {
                const resultado = [];
                data.forEach((item) => {
                    resultado.push({ ano: item._id, total: item.total });
                });
                res.json(resultado);
            }
            else {
                funcoesUtils.handlerError(res, 200, mensagens.sucesso.nenhumResultado);
            }
        }
    });
};

