const Bolsa = require('../models/bolsa.model');
const mensagens = require('../utils/mensagens');
const funcoesUtils = require('../utils/funcoesUtils');

exports.sincronizar = (req, res, dados) => {

    const bolsas = [];
    dados.forEach(item => {
        bolsas.push({
            url: item.url,
            uuid: item.uuid,
            categoria_bolsa: item.categoria_bolsa,
            aluno: item.aluno,
            valor_bolsa: item.valor_bolsa,
            setor: item.setor
        });
    });

    Bolsa.collection.insertMany(bolsas).then(() => {
        res.status(200).json({ mensagem: mensagens.sucesso.sincronizar });
    }).catch(error => {
        console.log('errro durante a sincronização>>>>', error);
        funcoesUtils.handlerError(res, 500, mensagens.errors.sincronizar);
    });
};


exports.consultarValoresBolsas = (res) => {
    Bolsa.aggregate([{ '$group': { _id: { categoria: '$categoria_bolsa', valor: '$valor_bolsa' }, total: { $sum: 1 } } }], function (err, data) {
        if (err) {
            console.log('error:', JSON.stringify(err));
            res.json({ mensangem: mensagens.errors.generico });
            throw err;
        } else {
            if (funcoesUtils.validarRetornoConsulta(data)) {
                const retorno = [];
                const  dataTotalizado =  data.map(item => ({
                    categoria: item._id.categoria.trim(), 
                    valorTotal: (Number(item._id.valor.replace(',', '.') * Number(item.total)))
                }));

                dataTotalizado.forEach(d => {
                    const index = retorno.findIndex( r => r.categoria === d.categoria);                        
                    if (retorno.length === 0 || index === -1) {
                        retorno.push(d);
                    } else {
                        let itemUpdate = retorno[index];
                        itemUpdate.valorTotal += d.valorTotal;
                        retorno[index] = itemUpdate;
                    }
                });
                res.send(retorno);
            } else {
                funcoesUtils.handlerError(res, 200, mensagens.sucesso.nenhumResultado);
            }
        }
    });
};
