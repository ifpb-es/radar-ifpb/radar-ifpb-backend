const mongoose = require('mongoose');
const { Schema } = mongoose;

const patrimonioSchema = new Schema({
  url: String,
  uuid: String,
  numero: Number,
  descricao: String,
  situacao: String,
  estado_conservacao: String,
  campus: Object,
  valor_inicial: Number,
  valor_liquido_contabil: Number,
  sincronizado: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('patrimonio', patrimonioSchema);