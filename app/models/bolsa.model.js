
const mongoose = require('mongoose');
const { Schema } = mongoose;

const bolsaSchema = new Schema({
  url: String,
  uuid: String,
  categoria_bolsa: String,
  aluno: String,
  valor_bolsa: Number,
  setor: String,
  sincronizado: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('bolsa', bolsaSchema);