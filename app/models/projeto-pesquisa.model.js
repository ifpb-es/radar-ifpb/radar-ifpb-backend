const mongoose = require('mongoose');
const { Schema } = mongoose;

const  projetoPesquisaSchema = new Schema({
    url: String,
    uuid: String,
    titulo: String,
    uo: Object,
    inicio_execucao: Date,
    fim_execucao: Date,
    foco_tecnologico: String,
    area_conhecimento: String,
    resumo: String,
    justificativa: String,
    participantes: Object,
    valor_total_executado: Number,
    aprovado: Boolean,
    sincronizado: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('projetoPesquisa', projetoPesquisaSchema);
