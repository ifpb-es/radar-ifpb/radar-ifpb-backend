const mongoose = require('mongoose');
const { Schema } = mongoose;

const programaSchema = new Schema({
  url: String,
  uuid: String,
  titulo: String,
  numero_atendimento: Number,
  atendimento_por_campus: Object,
  periodos_inscricao_por_campus: Object,
  sincronizado: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('programa', programaSchema);