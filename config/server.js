/** importar o módulo do framework express*/
const express = require('express');
const cors = require('cors');

/**importar o módulo do body-parser*/
const bodyParser = require('body-parser');

/** Importe das configurações do mongo*/
const db = require('./db');

/** Import configurações do cors*/
const corsOpts = require('./cors-config');

/** Importe swagger */
const swaggerUi = require('swagger-ui-express');
const swagger = require('./swagger');

const atividadesEstudantis = require('../app/routes/atividades-estudantis-router');
const projetosPesquisa = require('../app/routes/projetos-pesquisa-router');
const projetosExtensao = require('../app/routes/projetos-extensao-router');
const administracao = require('../app/routes/administracao-router');
const info = require('../app/routes/info-router');

const baseUrl = '/api/v1';

const server = express();

server.get('/swagger.json', (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  res.send(swagger);
});

/**configurar o middleware do body-parser */
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());
server.use(cors(corsOpts));
server.use(baseUrl, info);
server.use(baseUrl, atividadesEstudantis);
server.use(baseUrl, projetosPesquisa);
server.use(baseUrl, projetosExtensao);
server.use(baseUrl, administracao);
server.use(`${baseUrl}/docs`, swaggerUi.serve, swaggerUi.setup(swagger));

module.exports = server;