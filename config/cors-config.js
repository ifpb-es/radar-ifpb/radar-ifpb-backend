const corsOpts = {
    origin: '*',
    methods: [
      'GET',
      'POST',
      'PUT',
      'DELETE'
    ],
    allowedHeaders: [
      'Content-Type',
      'Origin', 
      'X-Requested-With', 
      'Accept'
    ],
  };

  exports = corsOpts;