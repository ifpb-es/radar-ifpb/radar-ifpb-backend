const config = module.exports
const PRODUCTION = process.env.NODE_ENV === 'desenv'

config.express = {
  port: 3000,
  ip: 'localhost'
}

if (PRODUCTION) {
  // for example
  config.express.ip = '0.0.0.0'
}
