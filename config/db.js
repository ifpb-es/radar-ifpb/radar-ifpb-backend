const mongoose = require('mongoose');

const dbConfig = {
  MONGO_USERNAME: 'root',
  MONGO_PASSWORD: 'r@D@41f', 
  MONGO_HOSTNAME: '127.0.0.1',
  MONGO_PORT: '27017',
  MONGO_DB: 'radarif'
};

const options = {
  useNewUrlParser: true,
  reconnectTries: Number.MAX_VALUE,
  reconnectInterval: 500,
  connectTimeoutMS: 10000
};

const url = `mongodb://db:${dbConfig.MONGO_PORT}/${dbConfig.MONGO_DB}`;
console.log(">>>>>> ",url);

mongoose.connect(url, options).then( function() {
  console.log('MongoDB is connected');
})
.catch( function(err) {
  console.log(err);
});
