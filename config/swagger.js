const swaggerJsdoc = require('swagger-jsdoc');

const  swaggerDefinition = {
  info: {
    description: 'API do backend do projeto Radar IFPB',
    swagger: '2.0',
    title: 'Radar IFPB API',
    version: '0.0.1',
  },
  servers:['http://localhost:3000'],
  basePath: '/api/v1'
};

const options = {
  swaggerDefinition: swaggerDefinition,
  apis: ['./**/routes/*.js'],
};

module.exports = swaggerJsdoc(options);