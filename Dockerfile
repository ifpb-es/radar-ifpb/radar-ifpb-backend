FROM node:11-alpine
LABEL maintainer="Pedro Pacheco <pedrovcpacheco@yahoo.com>"
ENV NODE_ENV development
RUN apk add git  \  
 &&  git clone https://gitlab.com/ifpb-es/radar-ifpb/radar-ifpb-backend.git 
WORKDIR /radar-ifpb-backend
RUN npm i && npm install nodemon -g 
EXPOSE 3000
CMD nodemon





